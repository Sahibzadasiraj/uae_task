import 'package:flutter/material.dart';
import 'package:uae_task/widget/home_container.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  var textStyle =
      TextStyle(color: Colors.black, fontSize: 30, fontWeight: FontWeight.bold);
//this is branch code
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        actions: <Widget>[
          Icon(
            Icons.search,
            size: 30,
            color: Colors.grey,
          ),
          SizedBox(
            width: 10,
          ),
          Padding(
            padding: const EdgeInsets.only(right: 8.0),
            child: Icon(Icons.notifications_none, color: Colors.grey, size: 30),
          )
        ],
      ),
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.height * 0.01),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Text(
                "Find amazing",
                style: textStyle,
              ),
            ),
            SizedBox(height: size.height * 0.007),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Text(
                "proposals happenings",
                style: textStyle,
              ),
            ),
            SizedBox(height: size.height * 0.007),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Text(
                "around you",
                style: textStyle,
              ),
            ),
            SizedBox(height: size.height * 0.01),
            Padding(
              padding: const EdgeInsets.only(left: 15.0),
              child: Text(
                "321 people like this",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.grey),
              ),
            ),
            SizedBox(height: size.height * 0.01),
            HomeContainer(
              size: size,
            ),
            SizedBox(
              height: 5,
            )
          ],
        ),
      ),
    );
  }
}

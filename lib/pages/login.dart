

import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter/material.dart';
import 'package:uae_task/pages/nav.dart';
import 'package:uae_task/widget/login_widget.dart';


class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool showPassword = true;
  String errorMessage;
  String email, password;
  bool isLoginPage=true;
  bool checkedValue=false;
  final formLogin = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    double _width = MediaQuery.of(context).size.width;
    var _txtStyle = TextStyle(
        fontFamily: "Sofia",
        fontWeight: FontWeight.w600,
        color: Colors.black54,
        fontSize: 16.0);
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
       leading:InkWell(
         onTap: (){
           setState(() {
             isLoginPage=!isLoginPage;
           });
         },
         child:  Icon(Icons.keyboard_backspace,color: Colors.grey,),
       ),
        actions: <Widget>[
          !isLoginPage?
          Center(child: Padding(padding: EdgeInsets.only(right: 5),
          child: Text("Do it later",
            style: TextStyle(color: Colors.grey),),),):Container(),
        ],
      ),
      body: Container(

          width: double.infinity,
          height: MediaQuery.of(context).size.height,
          child:Container(
            /// Set gradient black in image splash screen (Click to open code)

            child: SingleChildScrollView(
              child: Container(
                color: Colors.white,
                child: Form(
                  key: formLogin,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[

                      SizedBox(height:

                      MediaQuery.of(context).size.height*0.005,),
                      Padding(
                        padding: const EdgeInsets.only(left:15.0),
                        child: Align(
                          alignment: Alignment.topLeft,
                          child: isLoginPage?Text(
                              "Login",
                            style: TextStyle(
                              fontFamily: "Lemon",
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 24,)
                          ):
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                  "Personal Info",
                                  style: TextStyle(
                                    fontFamily: "Lemon",
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 24,)
                              ),
                             Container(
                               padding: EdgeInsets.only(right: 10),
                               width: MediaQuery.of(context).size.width*0.2,
                               height: 20,
                               child:  Row(
                                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                 children: <Widget>[
                                   CircleAvatar(
                                     radius: 3,backgroundColor: Colors.grey,
                                   ),
                                   CircleAvatar(
                                     radius: 3,backgroundColor: Colors.grey[300],
                                   ),CircleAvatar(
                                     radius: 3,backgroundColor: Colors.grey[300],
                                   ),CircleAvatar(
                                     radius: 3,backgroundColor: Colors.grey[300],
                                   ),CircleAvatar(
                                     radius: 3,backgroundColor: Colors.grey[300],
                                   ),
                                 ],
                               ),
                             ),
                            ],
                          ),
                        ),
                      ),

                     
                      isLoginPage?Container():SizedBox(
                        height: 35,
                      ),
                      isLoginPage?Container():
                          Align(
                            alignment: Alignment.topLeft,
                            child: Padding(padding: EdgeInsets.only(left: 15),
                            child: Text("Personal Info",
                            style: TextStyle(fontWeight: FontWeight.bold),),)
                          ),
                     isLoginPage?SizedBox(): SizedBox(
                        height: 20,
                      ),
                      isLoginPage
                          ? SizedBox(
                        height: MediaQuery.of(context).size.height*0.25,
                      )
                          : Container(),
                      isLoginPage
                          ? Container()
                          :  TextFromField(
                        icon: Icons.person,
                        hidePassword: false,
                        title: "First Name",
                        num: 1,
                        inputType: TextInputType.text,
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      TextFromField(
                        icon: Icons.email,
                        hidePassword: false,
                        title:isLoginPage?"Email": "Last Name",
                        num: 2,
                        inputType: TextInputType.text,
                      ),
                      isLoginPage
                          ? Container()
                          : SizedBox(
                        height: 10.0,
                      ),
                      isLoginPage?Container():
                      TextFromField(
                        icon: Icons.email,
                        hidePassword: false,
                        title: "BirthDay",
                        num: 3,
                        inputType: TextInputType.text,
                      ),
                      isLoginPage
                          ? Container()
                          : SizedBox(
                        height: 25.0,
                      ),
                      isLoginPage?Container():  Padding(padding: EdgeInsets.symmetric(horizontal: 15,vertical: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                              "Personal Info",
                              style: TextStyle(
                                fontFamily: "Lemon",
                                fontWeight: FontWeight.bold,
                                color: Colors.black,
                                fontSize: 18,)
                          ),
                        Container(
                           width: MediaQuery.of(context).size.width*0.31,
                           child:  Row(
                             children: <Widget>[
                               Text(
                                   "+Email",
                                   style: TextStyle(
                                     fontFamily: "Lemon",
                                     fontWeight: FontWeight.bold,
                                     color: Colors.deepPurple,
                                     fontSize: 16,)
                               ),
                               SizedBox(width: 10,),
                               Text(
                                   "+Phone",
                                   style: TextStyle(
                                     fontFamily: "Lemon",
                                     fontWeight: FontWeight.bold,
                                     color: Colors.deepPurple,
                                     fontSize: 16,)
                               ),
                             ],
                           ),
                         )
                        ],
                      ),),
                     isLoginPage?Container(): SizedBox(height: 10,),
                      isLoginPage
                          ? Container()
                          : TextFromField(
                        icon: Icons.email,
                        hidePassword: false,
                        title: "Email",
                        num: 2,
                        inputType: TextInputType.phone,
                      ),
                     isLoginPage?Container(): SizedBox(
                        height: 14.0,
                      ),
                      isLoginPage
                          ? Container()
                          : TextFromField(
                        icon: Icons.perm_contact_calendar,
                        hidePassword: false,
                        title: "contact",
                        num: 4,
                        inputType: TextInputType.phone,
                      ),
                      SizedBox(
                        height: 14.0,
                      ),
                      isLoginPage?TextFromField(
                        icon: Icons.enhanced_encryption,
                        hidePassword: true,
                        title: "Password",
                        num: 5,
                        inputType: TextInputType.text,
                      ):Container(),
                      isLoginPage
                          ? Container()
                          : SizedBox(
                        height: 14.0,
                      ),

                      SizedBox(
                        height: 10,
                      ),




                      isLoginPage
                          ?
                      InkWell(
                        onTap: () {
//                            Navigator.of(context).push(PageRouteBuilder(
//                                pageBuilder: (_, __, ___) =>  ForgotPasswordEmail()));
                        },
                        child: Container(
                            height: 50,
                            width: MediaQuery.of(context).size.width,
padding: EdgeInsets.only(right: 20),
                            child:Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Container(
                                  height: 40,width: 200,
                                  child: CheckboxListTile(
                                    dense: true,
                                    title: Text("Remember me"),
                                    value: checkedValue,
                                    onChanged: (newValue) {
                                      setState(() {
                                          checkedValue = newValue;
                                      });
                                    },
                                    controlAffinity: ListTileControlAffinity.leading,  //  <-- leading Checkbox
                                  ),
                                ),
                                Text(
                                  'Forget Password?',
                                  style: TextStyle(
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            )
                        ),
                      )
                          : Container(),

                    isLoginPage?Container():  Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Connect Your Social Accout',
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                          SizedBox(width: 10,),
                          Text(
                            'optional',
                            style: TextStyle(
                              color: Colors.grey[600],
                              fontSize: 10,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                     isLoginPage?Container(): SizedBox(
                        height: 30,
                      ),
                      InkWell(
                        onTap: isLoginPage?() {
setState(() {
  isLoginPage=!isLoginPage;
});

                        }:(){
Navigator.of(context).push(MaterialPageRoute(builder: (context)=>MainScreen()));
                        },
                        child: Container(
                          height:60,
                          width: MediaQuery.of(context).size.width *0.8,
                          decoration: BoxDecoration(
                            color:Theme.of(context).primaryColor,
                            borderRadius:
                            BorderRadius.all(Radius.circular(6.0)),
                            boxShadow: [
                              BoxShadow(
                                  color: Colors.black45.withOpacity(0.15),
                                  blurRadius: 19.0,
                                  spreadRadius: 2.0)
                            ],
                          ),
                          child: Center(
                            child: Text(
                              isLoginPage
                                  ? "Login"
                                  : "Apply & Next",
                              style: _txtStyle.copyWith(
                                  color: Colors.white, fontSize: 16.0),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 8,
                      ),
                     isLoginPage? Text("or connect with"):Container(),
                      SizedBox(
                        height: 8,
                      ),
                   isLoginPage?   Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            height: 50,width: 50,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.blue)
                            ),
                            child: Text("f",style: TextStyle(
                              color: Colors.blue,
                              fontSize: 22,
                              fontWeight: FontWeight.bold
                            ),),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Container(
                            alignment: Alignment.center,
                            height: 50,width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.red)
                            ),
                            child: Text("G+",style: TextStyle(
                                color: Colors.red,
                                fontSize: 20,
                                fontWeight: FontWeight.bold
                            ),),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          Container(alignment: Alignment.center,
                            height: 50,width: 50,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(5),
                                border: Border.all(color: Colors.blueAccent)
                            ),
                            child: Image.network(
                                "https://www.freeiconspng.com/uploads/logo-twitter-icon-symbol-0.png",
                            height: 20,width: 20,
                            ),
                          ),
                        ],
                      ):Container(),
                    isLoginPage?  InkWell(
                        onTap: () {


                          setState(() {
                            isLoginPage = !isLoginPage;
                          });


                        },
                        child: SizedBox(
                          width: _width,
                          child: Container(
                            padding: EdgeInsets.only(
                              right: 0,
                              top: 15,
                            ),
                            alignment: Alignment.center,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  isLoginPage
                                      ? 'Don\'t have account? '
                                      : 'Already have account? ',
                                  style: TextStyle(
                                    color: Colors.black,
                                  ),
                                ),
                                Text(
                                  isLoginPage
                                      ? 'Sign Up.'
                                      : 'Sign In.',
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ):Container(),


                    ],
                  ),
                ),

              ),
            ),
          ),),


    );
  }

  bool validationFOrmLogin() {
    var form = formLogin.currentState;
    if (form.validate()) {
      setState(() {
        form.save();
      });
      return true;
    }
    else if (!form.validate()) {
      return false;
    }
    return false;
  }

  waiting() {
    Center(child: CircularProgressIndicator(
      backgroundColor: Colors.red, semanticsLabel: "loafing",));
    print("aya tha");
  }



  Widget portraitHeader() {
    return Column(
      children: <Widget>[
        Hero(
          tag: "image_hero",
          child: Image.asset(
            "images/login/arrow.png",
            height: 44,
            width: 46,
          ),
        ),

        // Text(
        //   "Expert Beautician at your Home",
        //   style: TextStyle(
        //       fontSize: (20 / 7.6) * SizeConfig.textMultiplier,
        //       color: Colors.black),
        // )
      ],
    );
  }



}

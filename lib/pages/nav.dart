import 'package:flutter/material.dart';

import '../pages/home_page.dart';

class MainScreen extends StatefulWidget {
  //final Datum fooddata;

  //MainScreen({this});

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int currentTab = 0;
//this is branch code
  // Pages
  HomePage homePage;
  Widget historyPage;
  Widget profilePage;
  Widget setting;

  List<Widget> pages;
  Widget currentPage;
  bool isloggedin = false;

  @override
  void initState() {
    homePage = HomePage();
    historyPage = Center(
      child: Text("text"),
    );
    profilePage = Center(
      child: Text("texttt"),
    );
    setting = Center(
      child: Text("textttttttt"),
    );
    pages = [homePage, historyPage, profilePage, setting];
    currentPage = homePage;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      resizeToAvoidBottomPadding: false,
      bottomNavigationBar: BottomNavigationBar(
          currentIndex: currentTab,
          onTap: (index) {
            setState(() {
              currentTab = index;
              currentPage = pages[index];
            });
          },
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
              ),
              title: Text(" "),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.perm_contact_calendar,
              ),
              title: Text(" "),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.message,
              ),
              title: Text(" "),
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.person_outline,
              ),
              title: Text(" "),
            ),
          ]),
      body: currentPage,
    );
  }
}

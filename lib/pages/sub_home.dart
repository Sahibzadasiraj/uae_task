import 'package:flutter/material.dart';
import 'package:uae_task/widget/sub_home.dart';

class SubHome extends StatefulWidget {
  @override
  _SubHomeState createState() => _SubHomeState();
}

class _SubHomeState extends State<SubHome> {
  var textStyle =
      TextStyle(color: Colors.black, fontSize: 30, fontWeight: FontWeight.bold);
//this is branch code
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        height: size.height,
        width: size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.height * 0.03),
            Container(
              width: size.width,
              height: size.height * 0.425,
              child: Stack(
                children: <Widget>[
                  Container(
                    height: size.height * 0.34,
                    width: size.width,
                    decoration: BoxDecoration(
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image:
                                AssetImage("lib/pages/assets/google_map.jpg"))),
                  ),
                  Positioned(
                    bottom: 2,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Container(
                        decoration: BoxDecoration(
                            boxShadow: [
                              BoxShadow(color: Colors.grey, blurRadius: 2)
                            ],
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(5)),
                        height: size.height * 0.13,
                        width: size.width * 0.9,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.all(4),
                              width: 100,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8),
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: AssetImage(
                                          "lib/pages/assets/house.jpg"))),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("Unamed Road"),
                                SizedBox(
                                  height: 10,
                                ),
                                Text("L'viv L'viv oblast, 79000"),
                                SizedBox(
                                  height: 10,
                                ),
                                Text("49.215465 , 24.025133"),
                              ],
                            ),
                            Container(
                              width: 50,
                              height: 50,
                              child: Image.asset("lib/pages/assets/arrow.PNG"),
                            )
                          ],
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(height: size.height * 0.01),
            SubHomeContainer(size: size),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              height: 55,
              alignment: Alignment.center,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Icon(
                    Icons.home,
                    color: Colors.blue,
                  ),
                  Icon(
                    Icons.perm_contact_calendar,
                    color: Colors.grey,
                  ),
                  Icon(
                    Icons.message,
                    color: Colors.grey,
                  ),
                  Icon(
                    Icons.person_outline,
                    color: Colors.grey,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

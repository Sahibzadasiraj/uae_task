import 'package:flutter/material.dart';
import 'package:uae_task/pages/sub_home.dart';

class HomeContainer extends StatelessWidget {
  final Size size;
  HomeContainer({this.size});
//this is branch code
  var colText =
      TextStyle(color: Colors.deepPurple, fontWeight: FontWeight.w600);

  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: size.width,
        decoration: BoxDecoration(
          color: Colors.deepPurple,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.height * 0.015),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(left: 20.0, top: 5),
                  child: Text(
                    "Join Up!",
                    style: TextStyle(
                        fontSize: 26,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(right: 15.0),
                  child: Text(
                    "December 24",
                    style: TextStyle(
                        fontSize: 12,
                        fontWeight: FontWeight.bold,
                        color: Colors.white),
                  ),
                ),
              ],
            ),
            SizedBox(height: size.height * 0.015),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 30),
              child: Text(
                "A guide for 4-6 people needed, it will be a walking tour of the Lviv's historic places with"
                "lunch and coffee breaks, Lviv Golden HorseJoej capathien mountains.",
                textAlign: TextAlign.justify,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey[300]),
              ),
            ),
            SizedBox(height: size.height * 0.015),
            Center(
              child: Text(
                "-Sightseeing tour  'Getting to know lviv'",
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            SizedBox(height: size.height * 0.04),
            Container(
              height: size.height * 0.22,
              width: size.width,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: size.height * 0.22,
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.yellow[300]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(
                            Icons.watch_later,
                            color: Colors.deepPurple,
                          ),
                          Text(
                            "11:00",
                            style: colText,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: size.height * 0.22,
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.blue[300]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(
                            Icons.people_outline,
                            color: Colors.deepPurple,
                          ),
                          Text(
                            "2.5",
                            style: colText,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: size.height * 0.22,
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(
                            Icons.public,
                            color: Colors.deepPurple,
                          ),
                          Text(
                            "English",
                            style: colText,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: size.height * 0.015),
            Expanded(
              child: Row(
                children: <Widget>[
                  Container(
                    width: size.width / 2.1,
                    alignment: Alignment.center,
                    decoration: BoxDecoration(
                      border: Border(
                          right: BorderSide(color: Colors.grey, width: 1),
                          top: BorderSide(color: Colors.grey, width: 1)),
                    ),
                    child: Text(
                      "REFUSE",
                      style: TextStyle(color: Colors.white, letterSpacing: 1),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      Navigator.of(context)
                          .push(MaterialPageRoute(builder: (_) => SubHome()));
                    },
                    child: Container(
                      alignment: Alignment.center,
                      width: size.width / 2.1,
                      decoration: BoxDecoration(
                        border: Border(
                            left: BorderSide(color: Colors.grey, width: 1),
                            top: BorderSide(color: Colors.grey, width: 1)),
                      ),
                      child: Text(
                        "ACCEPT",
                        style: TextStyle(color: Colors.white, letterSpacing: 1),
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    ));
  }
}

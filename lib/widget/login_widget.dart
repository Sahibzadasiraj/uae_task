import 'package:flutter/material.dart';

class TextFromField extends StatelessWidget {
  final bool hidePassword;
  final String title;
  final IconData icon;
  final TextInputType inputType;
  final int num;

  TextFromField(
      {this.title, this.num, this.icon, this.inputType, this.hidePassword});

final passwordController=TextEditingController();
bool showPassword=false;
  @override
  Widget build(BuildContext context) {
    return
      Padding(
        padding: EdgeInsets.symmetric(horizontal: 15),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.grey
            ),
              borderRadius: BorderRadius.circular(7),


          ),
height: MediaQuery.of(context).size.height*0.07,
          child: TextFormField(

            controller: num == 4 ? passwordController : null,
            onSaved: (value) {
              switch (num) {
                case 1:
                  String fullName = value.toString().trim();
                  break;
                case 2:
                 String email = value.toString().trim();
                  break;
                case 3:
                  String contact = value.toString().trim();
                  break;
                case 4:
                  String password = value.toString().trim();
                  break;
                default:
              }
            },
            validator: (value) {
              switch (num) {
                case 3:
                  return value.length< 8 ? "number should be 8 digit":null;
                case 5:
                  return value != passwordController.text
                      ? "Password should be match!"
                      : null;
                  break;
                default:
                  return value.isEmpty ? "Field required" : null;
              }
            },
            obscureText:hidePassword,
            
//            maxLength: num==3?8:null,
            decoration: InputDecoration(
              
                border: InputBorder.none,
                labelText: title,

                suffixIcon: num==3?Icon(Icons.date_range,color: Colors.deepPurple,):null,

                icon: num == 4
                    ? Container(
                    padding: EdgeInsets.only(top: 8,left: 8),
                    width: MediaQuery.of(context).size.width*0.2,
                    child: Row(
                      children: <Widget>[
                        Image.network(
                          'https://image.flaticon.com/icons/png/512/323/323301.png',
                          height: 25,
                          width: 20,
                        ),
                        SizedBox(width: 4),
                        Text(
                          '+971',
                          style: TextStyle(
                            fontSize: 12,
                            fontFamily: 'Lemon',
                          ),
                        ),
                      ],
                    ))
                    : SizedBox(),
                labelStyle: TextStyle(
                    fontSize: 15.0,
                    fontFamily: 'Sans',
                    letterSpacing: 0.3,
                    color: Colors.black38,
                    fontWeight: FontWeight.w600)),
            keyboardType: inputType,
          ),
        ),
      );
  }
}
import 'package:flutter/material.dart';

class SubHomeContainer extends StatelessWidget {
  final Size size;
  SubHomeContainer({this.size});
//this is branch code
  var colText =
      TextStyle(color: Colors.deepPurple, fontWeight: FontWeight.w600);
  @override
  Widget build(BuildContext context) {
    return Expanded(
        child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: size.width,
        decoration: BoxDecoration(
          color: Colors.deepPurple,
          borderRadius: BorderRadius.circular(15),
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: size.height * 0.03),
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 2),
              child: Text(
                "Join Up Lviv",
                style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold,
                    color: Colors.white),
              ),
            ),
            SizedBox(height: size.height * 0.01),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                "A guide for 4-6 people needed, it will be a\n walking tour of the Lviv's historic places",
                textAlign: TextAlign.justify,
                style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w400,
                    color: Colors.grey[300]),
              ),
            ),
            SizedBox(height: size.height * 0.02),
            Container(
              height: size.height * 0.19,
              width: size.width,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Container(
                      height: size.height * 0.19,
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.yellow[300]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(
                            Icons.watch_later,
                            color: Colors.deepPurple,
                          ),
                          Text(
                            "11:00",
                            style: colText,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: size.height * 0.19,
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.blue[300]),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(
                            Icons.people_outline,
                            color: Colors.deepPurple,
                          ),
                          Text(
                            "2.5",
                            style: colText,
                          ),
                        ],
                      ),
                    ),
                    Container(
                      height: size.height * 0.19,
                      width: size.width * 0.28,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5),
                          color: Colors.white),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Icon(
                            Icons.public,
                            color: Colors.deepPurple,
                          ),
                          Text(
                            "English",
                            style: colText,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(height: size.height * 0.02),
            Expanded(
              child: Container(
                alignment: Alignment.center,
                width: size.width,
                decoration: BoxDecoration(
                  border: Border(
                      left: BorderSide(color: Colors.white, width: 1),
                      top: BorderSide(color: Colors.white, width: 1)),
                ),
                child: Text(
                  "Send Message",
                  style: TextStyle(color: Colors.white, letterSpacing: 1),
                ),
              ),
            )
          ],
        ),
      ),
    ));
  }
}
